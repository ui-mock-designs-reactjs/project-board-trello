# Project overview dashboard

## Tech

Here are the list of different technologies/frameworks/libraries used:
- [React with TypeScript](https://reactjs.org/) - For building the frontend components
- [TailwindCSS](https://tailwindcss.com/) - CSS utility framework to build designs easily and quickly

## Installation
Install the dependencies and start the server.

```sh
npm i
npm start
```
## Screenshots
![Screenshot](https://gitlab.com/ui-mock-designs-reactjs/project-board-trello/-/raw/master/Screenshot%201.png?inline=false "Screenshot 1")