/* tslint:disable */
import React, { Component } from "react";
import { Container, Draggable } from "react-smooth-dnd";
import { applyDrag, generateItems } from "./utils";
import "./trello.scss";

const lorem = `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.`;

const columnNames = [
  "Lorem",
  "Ipsum",
  "Consectetur",
  "Eiusmod",
  "Column 5",
  "Column 6",
  "Column 7",
  "Column 8",
  "Column 9",
  "Column 10",
  "Column 11",
  "Column 12",
];

const cardColors = [
  "azure",
  "beige",
  "bisque",
  "blanchedalmond",
  "burlywood",
  "cornsilk",
  "gainsboro",
  "ghostwhite",
  "ivory",
  "khaki",
];
const pickColor = () => {
  let rand = Math.floor(Math.random() * 10);
  return cardColors[rand];
};

const indexClass = {
  "0": "one",
  "1": "two",
  "2": "three",
};

class Cards extends Component {
  constructor(props: any) {
    super(props);

    this.onColumnDrop = this.onColumnDrop.bind(this);
    this.onCardDrop = this.onCardDrop.bind(this);
    this.getCardPayload = this.getCardPayload.bind(this);
  }

  state = {
    trelloData: [
      {
        id: 1,
        header: "Yet to start",
        icon: `<svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8 12h.01M12 12h.01M16 12h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
              </svg>`,
        borderColor: "#f78e65",
        cards: [
          {
            id: 1,
            header: "Coolchat Platform UX/UI",
            company: "Cool Creatives",
            text: "Business Messenger that makes office environment cool again",
            members: ["PT", "CL", "BW"],
          },
          {
            id: 2,
            header: "Ethipoi Delivery App UX",
            company: "Carrom Inc",
            text: "User research and Usability testing for food delivery app",
            members: ["PT", "CL", "+5"],
          },
          {
            id: 3,
            header: "Coolchat Platform UX/UI",
            company: "Cool Creatives",
            text: "Business Messenger that makes office environment cool again",
            members: ["PT", "CL", "BW"],
          },
          {
            id: 4,
            header: "Ethipoi Delivery App UX",
            company: "Carrom Inc",
            text: "User research and Usability testing for food delivery app",
            members: ["PT", "CL", "+5"],
          },
          {
            id: 5,
            header: "Coolchat Platform UX/UI",
            company: "Cool Creatives",
            text: "Business Messenger that makes office environment cool again",
            members: ["PT", "CL", "BW"],
          },
          {
            id: 6,
            header: "Ethipoi Delivery App UX",
            company: "Carrom Inc",
            text: "User research and Usability testing for food delivery app",
            members: ["PT", "CL", "+5"],
          },
          {
            id: 7,
            header: "Coolchat Platform UX/UI",
            company: "Cool Creatives",
            text: "Business Messenger that makes office environment cool again",
            members: ["PT", "CL", "BW"],
          },
          {
            id: 8,
            header: "Ethipoi Delivery App UX",
            company: "Carrom Inc",
            text: "User research and Usability testing for food delivery app",
            members: ["PT", "CL", "+5"],
          },
        ],
      },
      {
        id: 2,
        header: "Active",
        icon: `<svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 8v4l3 3m6-3a9 9 0 11-18 0 9 9 0 0118 0z" />
              </svg>`,
        borderColor: "#4acbf2",
        cards: [
          {
            id: 53,
            header: "Maple website redesign",
            company: "Crayon Labs",
            text: "Phase 2 of Apple website design where we focus to improve branding.",
            members: ["PT", "CL", "BW"],
          },
          {
            id: 54,
            header: "Website design and development",
            company: "Babidas",
            text: "User research and Usability testing for food delivery app",
            members: ["PT", "CL", "+5"],
          },
        ],
      },
      {
        id: 3,
        header: "Completed",
        icon: `<svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z" />
              </svg>`,
        borderColor: "#8573e1",
        cards: [
          {
            id: 81,
            header: "Maple mobile app",
            company: "Crayon Labs",
            text: "Creating a mobile app for Maple, which helps deliver breakfast in offices.",
            members: ["CL", "PT"],
          },
          {
            id: 82,
            header: "Nacho CRM UI/UX",
            company: "Feather Labs",
            text: "The UI/UX for new flagship CRM for Feather Labs",
            members: ["CL", "PT"],
          },
          {
            id: 83,
            header: "Care Healthcare Platform",
            company: "Crayon Labs",
            text: "Design and development for patient management and healthcare platform.",
            members: ["CL", "PT"],
          },
        ],
      },
    ],
    scene: {
      type: "container",
      props: {
        orientation: "horizontal",
      },
      children: generateItems(12, (i: any) => ({
        id: `column${i}`,
        type: "container",
        name: columnNames[i],
        props: {
          orientation: "vertical",
          className: "card-container",
        },
        children: generateItems(
          +(Math.random() * 10).toFixed() + 5,
          (j: any) => ({
            // children: generateItems(3, (j: any) => ({
            type: "draggable",
            id: `${i}${j}`,
            props: {
              className: "card",
              style: { backgroundColor: pickColor() },
            },
            data: lorem.slice(0, Math.floor(Math.random() * 150) + 30),
          })
        ),
      })),
    },
  };

  render() {
    return (
      <div className="card-scene">
        <Container
          // dragBeginDelay={500}
          orientation="horizontal"
          onDrop={this.onColumnDrop}
          dragHandleSelector=".column-drag-handle"
          dropPlaceholder={{
            animationDuration: 150,
            showOnTop: true,
            className: "cards-drop-preview",
          }}
        >
          {this.state.trelloData.map((column) => {
            return (
              <Draggable key={column.id} className="trello-column">
                <div className="column-container">
                  <div style={{ padding: 10 }}>
                    <div
                      className="card-column-header"
                      style={{
                        borderBottom: `3px solid ${column.borderColor}`,
                      }}
                    >
                      <div
                        className="column-drag-handle column-icon"
                        dangerouslySetInnerHTML={{ __html: column.icon }}
                      ></div>
                      <div>
                        {column.header}
                        <span className="count">{column.cards.length}</span>
                      </div>
                    </div>
                  </div>
                  <Container
                    // dragBeginDelay={500}
                    orientation="vertical"
                    groupName="col"
                    onDragStart={(e) => console.log("drag started", e)}
                    onDragEnd={(e) => console.log("drag end", e)}
                    onDrop={(e) => this.onCardDrop(column.id, e)}
                    getChildPayload={(index) =>
                      this.getCardPayload(column.id, index)
                    }
                    dragClass="card-ghost"
                    dropClass="card-ghost-drop"
                    onDragEnter={() => {
                      console.log("drag enter:", column.id);
                    }}
                    onDragLeave={() => {
                      console.log("drag leave:", column.id);
                    }}
                    onDropReady={(p) => console.log("Drop ready: ", p)}
                    dropPlaceholder={{
                      animationDuration: 150,
                      showOnTop: true,
                      className: "drop-preview",
                    }}
                    // dropPlaceholderAnimationDuration={200}
                  >
                    {column.cards.map((card) => {
                      return (
                        <Draggable key={card.id}>
                          <div className="card">
                            <div className="card-header">{card.header}</div>
                            <div className="company">{card.company}</div>
                            <div className="text">{card.text}</div>
                            <div className="footer">
                              <div className="date">
                                <svg
                                  xmlns="http://www.w3.org/2000/svg"
                                  className="h-6 w-6"
                                  fill="none"
                                  viewBox="0 0 24 24"
                                  stroke="currentColor"
                                >
                                  <path
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                    strokeWidth="2"
                                    d="M8 7V3m8 4V3m-9 8h10M5 21h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v12a2 2 0 002 2z"
                                  />
                                </svg>
                                25/06/20 - 28/06/20
                              </div>
                              <div className="members">
                                {card.members.map((member, index) => (
                                  <div
                                    key={member}
                                    className={`member ${
                                      (indexClass as any)[index]
                                    }`}
                                  >
                                    {member}
                                  </div>
                                ))}
                              </div>
                            </div>
                          </div>
                        </Draggable>
                      );
                    })}
                  </Container>
                </div>
              </Draggable>
            );
          })}
        </Container>
      </div>
    );
  }

  getCardPayload(columnId: any, index: any) {
    console.log("Column ID: ", columnId, " Index: ", index);
    return this.state.trelloData.filter((column) => column.id === columnId)[0]
      .cards[index];
  }

  onColumnDrop(dropResult: any) {
    const scene = Object.assign({}, (this.state as any).scene);
    scene.children = applyDrag(scene.children, dropResult);
    this.setState({
      scene,
    });
  }

  onCardDrop(columnId: any, dropResult: any) {
    if (dropResult.removedIndex !== null || dropResult.addedIndex !== null) {
      const column = this.state.trelloData.filter(
        (column) => column.id === columnId
      )[0];
      const columnIndex = this.state.trelloData.indexOf(column);

      const newColumn = Object.assign({}, column);
      newColumn.cards = applyDrag(newColumn.cards, dropResult);
      this.state.trelloData.splice(columnIndex, 1, newColumn);

      this.setState({
        trelloData: this.state.trelloData,
      });
    }
  }
}

export { Cards };
