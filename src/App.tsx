import React from "react";
import "./App.scss";
import { Header } from "./components/header/header";
import { SideBar } from "./components/side-bar/side-bar";
import { Cards } from "./components/trello/trello";

function App() {
  return (
    <div className="App">
      <SideBar />
      <div className="main-app">
        <Header />
        <div className="trello">
          <Cards />
        </div>
      </div>
    </div>
  );
}

export default App;
